﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Test.Domain.Entities;

namespace Infrastructure
{
    internal sealed class RankHistoryStore : IRankHistoryStore
    {
        private readonly string _connectionString;

        public RankHistoryStore(IConfiguration configuration)
        {
            _connectionString = configuration.GetConnectionString("Local");
        }

        public async Task CreateAsync(RankHistory rankHistory)
        {
            using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            var command = new SqlCommand(@"
                        INSERT INTO [dbo].[RankHistory] ([CreatedDate], [Url], [Keywords], [Results]) 
                        VALUES (@CreatedDate, @Url, @Keywords, @Results)")
            {
                Connection = connection
            };

            command.Parameters.AddWithValue("@CreatedDate", rankHistory.CreateDate);
            command.Parameters.AddWithValue("@Url", rankHistory.Url);
            command.Parameters.AddWithValue("@Keywords", rankHistory.Keywords);
            command.Parameters.AddWithValue("@Results", rankHistory.Results);
            command.ExecuteNonQuery();
        }

        public async Task<IEnumerable<RankHistory>> GetAllAsync()
        {
            var entities = new List<RankHistory>();

            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                var command = new SqlCommand(@"SELECT * FROM [dbo].[RankHistory] ORDER BY [CreatedDate] DESC")
                {
                    Connection = connection
                };

                using var reader = command.ExecuteReader();

                while (reader.Read())
                {
                    var entity = new RankHistory
                    {
                        Id = reader.GetInt32(reader.GetOrdinal("Id")),
                        CreateDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate")),
                        Url = reader.GetString(reader.GetOrdinal("Url")),
                        Keywords = reader.GetString(reader.GetOrdinal("Keywords")),
                        Results = reader.GetString(reader.GetOrdinal("Results"))
                    };
                    entities.Add(entity);
                }
            }

            return entities;
        }
    }
}
