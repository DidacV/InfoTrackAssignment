﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Domain.Services;

namespace Test.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RankFinderController : ControllerBase
    {
        private readonly IRankFinder _rankFinder;

        public RankFinderController(IRankFinder rankFinder)
        {
            _rankFinder = rankFinder;
        }

        [HttpGet]
        public async Task<ActionResult> Get(string url, string keywords)
        {
            var res = await _rankFinder.FindRank(url, keywords);

            return new JsonResult(res);
        }

        [HttpGet]
        [Route("history")]
        public async Task<ActionResult> GetAll()
        {
            var res = await _rankFinder.GetHistory();

            return new JsonResult(res);
        }
    }
}
