import React, { Component } from 'react';

export class History extends Component {
  constructor(props) {
    super(props);
    this.state = { loading: false, results: [] };
  }

  async componentDidMount() {
    const endpoint = `rankfinder/history`;

    this.setState({ loading: true });

    const response = await fetch(endpoint);

    const data = await response.json();

    this.setState({ results: data, loading: false });
  }

  render() {
    const { loading, results } = this.state;

    return (
      <div className="text-center">
        <h1 className="display-4">History</h1>
        {loading ? (
          <div>Loading...</div>
        ) : (
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Created</th>
                <th scope="col">Url</th>
                <th scope="col">Keywords</th>
                <th scope="col">Results</th>
              </tr>
            </thead>
            <tbody>
              {results.map((x, i) => (
                <tr key={i}>
                  <td className="item">{x.createdDate}</td>
                  <td className="item">{x.url}</td>
                  <td className="item">{x.keywords}</td>
                  <td className="item">{x.results}</td>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}
