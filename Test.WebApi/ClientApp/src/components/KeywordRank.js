import React, { Component } from "react";

export class KeywordRank extends Component {
  constructor(props) {
    super(props);
    this.state = { url: "", keywords: "", result: "", loading: false };

    this.updateUrl = this.updateUrl.bind(this);
    this.updateKeywords = this.updateKeywords.bind(this);
    this.submit = this.submit.bind(this);
  }

  updateUrl(event) {
    this.setState({ url: event.target.value });
  }

  updateKeywords(event) {
    this.setState({ keywords: event.target.value });
  }

  async submit() {
    const { url, keywords } = this.state;

    const endpoint = `rankfinder/?url=${url}&keywords=${keywords}`;

    this.setState({ loading: true });

    const response = await fetch(endpoint);

    const data = await response.json();

    this.setState({ result: data, loading: false });
  }

  render() {
    const { url, keywords, result, loading } = this.state;

    return (
      <div className="text-center">
        <h1 className="display-4">Welcome</h1>
        <p>Enter your keywords and URL below</p>
        <div className="d-flex">
          <div className="col-5">
            <p>Url</p>
            <input
              type="text"
              value={url}
              placeholder="Url"
              onChange={this.updateUrl}
            />
          </div>
          <div className="col-5">
            <p>Keywords</p>
            <input
              type="text"
              value={keywords}
              placeholder="Keywords"
              onChange={this.updateKeywords}
            />
          </div>
          <div className="col-2 align-self-end">
            <button className="btn btn-primary" onClick={this.submit} disabled={loading}>
              Submit
            </button>
          </div>
        </div>
        <div className="card-body">
          <h4>Results:</h4>
          {loading ? <div>Loading...</div> : <span>{result}</span>}
        </div>
      </div>
    );
  }
}
