import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { KeywordRank } from './components/KeywordRank';
import { History } from './components/History';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={KeywordRank} />
        <Route exact path='/history' component={History} />
      </Layout>
    );
  }
}
