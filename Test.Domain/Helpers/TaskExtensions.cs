using System;
using System.Threading;
using System.Threading.Tasks;

namespace Test.Domain.Helpers
{
    public static class TaskHelpers
    {
        public static Task<T> StartStaTask<T>(Func<T> func)
        {
            var completionSource = new TaskCompletionSource<T>();
            Thread thread = new Thread(() =>
            {
                try
                {
                    completionSource.SetResult(func());
                }
                catch (Exception e)
                {
                    completionSource.SetException(e);
                }
            });
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
            return completionSource.Task;
        }
    }
}