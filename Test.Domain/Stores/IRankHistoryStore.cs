﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Domain.Entities;

namespace Infrastructure
{
    public interface IRankHistoryStore
    {
        Task<IEnumerable<RankHistory>> GetAllAsync();

        Task CreateAsync(RankHistory rankHistory);
    }
}
