﻿using System;

namespace Test.Domain.Entities
{
    public class RankHistory
    {
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public string Url { get; set; }

        public string Keywords { get; set; }

        public string Results { get; set; }
    }
}
