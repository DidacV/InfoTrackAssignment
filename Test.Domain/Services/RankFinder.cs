﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Domain.DTOs;
using Test.Domain.Entities;
using Test.Domain.Exceptions;
using Test.Domain.Services.Scraping;

namespace Test.Domain.Services
{
    internal class RankFinder : IRankFinder
    {
        private readonly IGoogleScraper _scraper;
        private readonly IRankHistoryStore _rankHistoryStore;

        public RankFinder(IGoogleScraper scraper, IRankHistoryStore rankHistoryStore)
        {
            _scraper = scraper;
            _rankHistoryStore = rankHistoryStore;
        }

        public async Task<string> FindRank(string url, string keywords)
        {
            var result = "0";

            if (string.IsNullOrEmpty(url) || string.IsNullOrEmpty(keywords))
            {
                return result;
            }

            try
            {
                var links = await _scraper.GetUrlsMatching(url, keywords);
                var positions = links.Select(x => x.Value).ToList();

                if (positions.Any())
                {
                    result = string.Join(", ", positions);
                }

                await _rankHistoryStore.CreateAsync(new RankHistory
                {
                    CreateDate = DateTime.UtcNow,
                    Keywords = keywords,
                    Url = url,
                    Results = result
                });
            }
            catch (TooManyRequestsException)
            {
                result = "Too many requests";
            }


            return result;
        }

        public async Task<IEnumerable<RankHistoryDTO>> GetHistory()
        {
            var dtos = new List<RankHistoryDTO>();

            var entities = await _rankHistoryStore.GetAllAsync();
            foreach (var entity in entities)
            {
                dtos.Add(new RankHistoryDTO
                {
                    CreatedDate = entity.CreateDate.ToString("dd/MM/yy - HH:mm"),
                    Url = entity.Url,
                    Keywords = entity.Keywords,
                    Results = entity.Results
                });
            }

            return dtos;
        }
    }
}
