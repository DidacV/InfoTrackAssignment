using System.Collections.Generic;
using System.Windows.Forms;

namespace Test.Domain.Services.Scraping
{
    public class DuckDuckGoScraper : Scraper, IDuckDuckGoScraper
    {
        protected override IEnumerable<HtmlElement> GetLinks(HtmlDocument document)
        {
            var refLinks = document.GetElementsByTagName("a");

            if (refLinks.Count == 0) return new List<HtmlElement>();
            
            var results = new List<HtmlElement>();
            
            foreach (HtmlElement element in refLinks)
            {
                ProcessChildren(element, results);
            }

            return results;
        }

        private void ProcessChildren(HtmlElement element, ICollection<HtmlElement> toAdd)
        {
            if (element.GetAttribute("className") == "result__url")
            {
                toAdd.Add(element);
            }

            if (!element.CanHaveChildren) return;

            foreach (HtmlElement elementChild in element.Children)
            {
                ProcessChildren(elementChild, toAdd);
            }
        }

        protected override string GetUrl(string query)
        {
            return $"https://duckduckgo.com/?q={query}";
        }
    }
}