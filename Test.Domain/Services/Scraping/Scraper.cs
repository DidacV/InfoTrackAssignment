using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Test.Domain.Helpers;

namespace Test.Domain.Services.Scraping
{
    public abstract class Scraper : IScraper
    {
        private HtmlDocument _document;

        public async Task<Dictionary<string, int>> GetUrlsMatching(string url, string keywords)
        {
            var refLinks = (await GetLinks(keywords)).ToList();

            var urlToMatch = url.ToLower();

            var result = new Dictionary<string, int>();
            var discarded = new HashSet<string>(); // to keep track of position in one pass

            var position = 0; // use position instead of index in case of duplicates
            for (int i = 0; i < refLinks.Count; i++)
            {
                var element = refLinks[i];
                var elementUrl = element.InnerText.ToLower();
                
                position++;
                if (!elementUrl.Contains(urlToMatch))
                {
                    var added = discarded.Add(elementUrl);
                    if (!added) position--;
                    continue;
                }

                if (result.ContainsKey(elementUrl))
                {
                    position--;
                    continue;
                }

                result.Add(elementUrl, position);
            }

            return result;
        }
        
        private async Task<IEnumerable<HtmlElement>> GetLinks(string keywords)
        {
            if (_document == null)
            {
                await LoadDocument(keywords);
            }

            return GetLinks(_document);
        }
        
        private async Task LoadDocument(string query)
        {
            var url = GetUrl(query);
            var uri = new Uri(url);
            _document = await TaskHelpers.StartStaTask(() => GetDoc(uri));
        }
        
        private HtmlDocument GetDoc(Uri uri)
        {
            using var wb = new WebBrowser { ScriptErrorsSuppressed = true };

            wb.Navigate(uri);
            while (wb.ReadyState != WebBrowserReadyState.Complete || wb.IsBusy)
            {
                Application.DoEvents();
                Thread.Sleep(TimeSpan.FromMilliseconds(100));
            }
            return wb.Document;
        }

        protected abstract IEnumerable<HtmlElement> GetLinks(HtmlDocument document);

        protected abstract string GetUrl(string query);
    }
}