using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Test.Domain.Exceptions;

namespace Test.Domain.Services.Scraping
{
    public class GoogleScraper : Scraper, IGoogleScraper
    {
        protected override IEnumerable<HtmlElement> GetLinks(HtmlDocument document)
        {
            var refLinks = document.GetElementsByTagName("cite");

            if (refLinks.Count == 0) return new List<HtmlElement>();

            var uri = document.Url;
            if (uri != null && uri.ToString().Contains("sorry"))
            {
                throw new TooManyRequestsException();
            }

            return refLinks.Cast<HtmlElement>();
        }

        protected override string GetUrl(string query)
        {
            return $"https://www.google.co.uk/search?num=100&q={query}";
        }
    }
}