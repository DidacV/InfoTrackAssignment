using System.Collections.Generic;
using System.Threading.Tasks;

namespace Test.Domain.Services.Scraping
{
    public interface IScraper
    {
        /// <summary>
        /// Matches the given url against the loaded document. Builds a dictionary with the matched URL and its position in the results. 
        /// </summary>
        /// <param name="url">URL to match against</param>
        /// <param name="keywords">query for google</param>
        Task<Dictionary<string, int>> GetUrlsMatching(string url, string keywords);
    }
}