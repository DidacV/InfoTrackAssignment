﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Test.Domain.DTOs;

namespace Test.Domain.Services
{
    public interface IRankFinder
    {
        /// <summary>
        /// Finds where in the first 100 results <paramref name="url" /> appears using <paramref name="keywords" /> to search.
        /// </summary>
        Task<string> FindRank(string url, string keywords);

        Task<IEnumerable<RankHistoryDTO>> GetHistory();
    }
}
