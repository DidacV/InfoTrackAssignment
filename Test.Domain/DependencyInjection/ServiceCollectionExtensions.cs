﻿using Microsoft.Extensions.DependencyInjection;
using Test.Domain.Services;
using Test.Domain.Services.Scraping;

namespace Test.Domain.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDomain(this IServiceCollection services)
        {
            services.AddTransient<IGoogleScraper, GoogleScraper>();
            services.AddTransient<IDuckDuckGoScraper, DuckDuckGoScraper>();
            services.AddTransient<IRankFinder, RankFinder>();
        }
    }
}
