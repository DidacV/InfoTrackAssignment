﻿namespace Test.Domain.DTOs
{
    public class RankHistoryDTO
    {
        public string CreatedDate { get; set; }

        public string Url { get; set; }

        public string Keywords { get; set; }

        public string Results { get; set; }
    }
}
