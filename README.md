# Before Running

Assuming you have SQL Server running locally...
Create database and history table by executing the CreateDatabase.sql and CreateTable.sql scripts respectively that are under Infrastructure/Scripts.

# App's connection string

You can find the app's connection string in appsettings.json's in the WebApi project.

# Running

Open the solution in Visual Studio 2019 and press Debug. VS should install the npm dependencies from the React project and launch IISExpress.

# Note

The app uses WebBrowser from WPF to query the DOM.
